const choo = require('choo')
const app = choo()
var PouchDB = require('pouchdb-core')

PouchDB.plugin(require('pouchdb-adapter-idb'))
  .plugin(require('pouchdb-adapter-websql'))
  .plugin(require('pouchdb-adapter-http'))
  .plugin(require('pouchdb-replication'))

var db = new PouchDB('spass')

app.model({
  state: { doc: null },
  reducers: {
    update: (action, state) => ({ doc: action.doc })
  },
  effects: {
    'lookup-site': (action, state, send) => {
      db.get(action.value).then(function (doc) {
        send('update', {doc})
      }).catch(function (err) {})
    },
    'save': (action, state, send) => {
      var doc
      action.data.forEach((entry, key) => {
        console.log(entry, key)
      })
    }
  }
})

const mainView = (params, state, send) => {
  return choo.view`
    <main>
      <h1>SGP</h1>
      <form onsubmit=${onsubmit}>
        <input name="_id" type="text" placeholder="website" oninput=${(e) => send('lookup-site', { value: e.target.value })} />
        <input name="pw" type="password" placeholder="Master password" />
        ${otherFields(state.doc)}
        <input type="submit" text="Save"/>
      </form>
    </main>
  `
  function onsubmit (event) {
    console.log(event.target.elements[''])
    send('save', { data: new FormData(event.target) })
    event.preventDefault()
  }
}

const otherFields = (doc) => {
  if (!doc) return emptyDoc()
  else return showDoc(doc)
}

const emptyDoc = () => choo.view`
  <div>
    <input name="username" type="text" placeholder="username" />
    <input name="suffix" type="text" placeholder="password suffix" />
  </div>
`

const showDoc = (doc) => choo.view`
  <div>
    <h3>${doc.username}</h3>
    <input type="password" placeholder="Master password" value="${doc.username}" />
  </div>
`

app.router((route) => [
  route('/', mainView)
])

if (module.parent) module.exports = app
else document.body.appendChild(app.start())
